import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image} from 'react-native';
import Login from './components/Login';
import Counter from './components/counter/Counter';
import Waiter from './components/waiter/Waiter';
import { createStackNavigator} from 'react-navigation';

export default class App extends Component {
  constructor(){
    super();
    this.state={
      isVisible : true,
    }
  }
  Hide_Splash_Screen=()=>{
    this.setState({
      isVisible : false
    });
  }

  componentDidMount(){
    var that = this;
    setTimeout(function(){
      that.Hide_Splash_Screen();
    }, 3000);
  }
  render() {
    return (
      (this.state.isVisible === true) ? <Splash/> : <RootStack/>
    );
  }
}
class Splash extends Component {

  render() {
      return(
          <View style={styles.container}>
             <Image style={styles.image} source={require('./images/my.jpg')}>
             </Image>
          </View>
          
      )
  } 
}
const styles = StyleSheet.create({
  container: {
      backgroundColor: '#211',
      alignItems : 'center',
      justifyContent : 'center',
      flex : 1
  },
  image: {
      width: '100%',
      height: '100%',
     resizeMode: 'stretch'
  }
})
const RootStack = createStackNavigator(
  {
    Login: {
      screen: Login,
      overrideBackPress: true
    },
    Counter : {
      screen : Counter,
      overrideBackPress: true
    },
    Waiter : {
      screen : Waiter,
      overrideBackPress: true
    }
  },
  {
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    },
    initialRouteName: 'Login',
  }
);
