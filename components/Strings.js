import LocalizedStrings from 'react-native-localization';
 
module.exports  = new LocalizedStrings({
    en: {
        "BASE_URL" : "http://192.168.1.173/api/",
        "BASE_URL1" : "http://192.168.1.173",
        "SEND" : "Send",
        "CREATE" : "Create",
        "NAME" : "Name",
        "PRICE" : "Price",
        "ORDER" : "Order",
        "ID" : "ID",
        "CREATED_DATE" : "Created Date",
        "PAID_DATE" : "Paid Date",
        "AMOUNT" : "Amount",
        "DESCRIPTION" : "Description",
        "TOTAL": "Total",
        "TABLE": "Table",
        "ORDERED_AT": "Ordered at",
        "PAID_AT": "Paid at",
        "YES": "Yes",
        "NO": "No",
        "SURE": "Are you sure?",
        "CONFRIM": "Confirm",
        "ASK_AGREE": "Do you agree?",
        "OK": "OK",
        "CANCEL": "Cancel",
        "ABORD": "Abort",
        "RETRY": "Retry",
        "IGNORE": "Ignore",
        "AGREE": "Agree",
        "DISAGREE": "Disagree",
        "DONT_AGREE": "Don't agree",
        "UPLOAD_DONE": "Successfully uploaded.",
        "UPLOAD_FAIL": "Upload failed",
        "RETURN": "Return",
        "BACK": "Back",
        "FORWARD": "Forward",
        "NEXT": "Next",
        "PREVIEW": "Preview",
        "AFTER": "After",
        "TAKE_PICTURE": "Take a picture",
        "CAPTURE": "Capture",
        "TAKE": "Take",
        "CAMERA": "Camera",
        "CAPTURE_FAIL": "Capture failed",
        "NOT_EXIST": "Not exist",
        "NO_FOLDER": "No such Folder",
        "NO_DIRECTORY": "No such directory",
        "INVALID_PATH": "Invalid path",
        "OPEN": "Open",
        "SAVE": "Save",
        "NUMBER": "#",
        "ADD" :"Add",
        "CATEGORY" : "Category",
        "PRODUCT" : "Product",
        "FROM_IMAGE": "Where do you want image from?",
        "CHOOSE_ACTION": "Choose an action",
        "FROM_CAMERA": "Take from Camera",
        "FROM_GALLERY": "Take from Gallery",
        "WAIT": "Please Wait....",
        "INSERT_CATEGORY_NAME": "please insert category name",
        "UPLOAD_CATEGORY_IMAGE": "please upload image of category",
        "SURE_ACCEPT_ORDER" : "Do you agree to accept this order?",
        "SURE_ORDER_PAID": "Are you sure that this order is paid?",
        "SURE_DELIVER_COFFEE": "Did you deliver the coffee?",
        "CONFIRM": "Confirm",
        "INSERT_PRODUCT_NAME": "please insert product's name",
        "INSERT_PRODUCT_PRICE": "please insert product's price",
        "UPLOAD_PRODUCT_IMAGE": "please upload product's image",
        "UPDATE" : "Update",
        "DELETE"  : "Delete",
        "DELIVERED" : "Delivered",
        "PAID " : "Paid",
        "SIGNIN" : "SIGN IN",
        "ENTERUSERNAME" : "Enter Username",
        "ENTERPASSWORD" : "Enter Password",
        "LESS" : "Less",
        "MORE" : "More",
    },
    kh: {
        "BASE_URL" : "http://192.168.1.173/api/",        
        "BASE_URL1" : "http://192.168.1.173",
        "SEND" : "ផ្ញើ",
        "CREATE" : "បង្កើត",
        "NAME" : "ឈ្មោះ",
        "PRICE" : "តំលៃ",
        "ORDER" : "លំដាប់",
        "ID" : "លេខសម្គាល់",
        "CREATED_DATE" : "បានបង្កើតកាលបរិច្ឆេទ",
        "PAID_DATE" : "កាលបរិច្ឆេទបង់ប្រាក់",
        "AMOUNT" : "ចំនួន",
        "DESCRIPTION" : "ការពិពណ៌នា",
        "TOTAL": "សរុប",
        "TABLE": "តារាង",
        "ORDERED_AT": "បញ្ជានៅ",
        "PAID_AT": "បង់នៅ",
        "YES": "បាទ",
        "NO": "ទេ",
        "SURE": "តើ​អ្នក​ប្រាកដ​ឬ​អត់?",
        "CONFRIM": "អះអាង",
        "ASK_AGREE": "តើអ្នកយល់ស្របទេ?",
        "OK": "យល់ព្រម",
        "CANCEL": "បោះបង់",
        "ABORD": "បោះបង់",
        "RETRY": "ព្យាយាមម្តងទៀត",
        "IGNORE": "មិនអើពើ",
        "AGREE": "យល់ព្រម",
        "DISAGREE": "មិនយល់ស្រប",
        "DONT_AGREE": "មិនយល់ស្រប",
        "UPLOAD_DONE": "បានផ្ទុកឡើងដោយជោគជ័យ។",
        "UPLOAD_FAIL": "ផ្ទុកឡើងបានបរាជ័យ",
        "RETURN": "ត្រឡប់",
        "BACK": "ថយក្រោយ",
        "FORWARD": "បន្ត",
        "NEXT": "បន្ទាប់",
        "PREVIEW": "មើលជាមុន",
        "AFTER": "បន្ទាប់ពី",
        "TAKE_PICTURE": "ថតរូប",
        "CAPTURE": "ចាប់យក",
        "TAKE": "យក",
        "CAMERA": "កាមេរ៉ា",
        "CAPTURE_FAIL": "ការចាប់យកបានបរាជ័យ",
        "NOT_EXIST": "មិនមាន",
        "NO_FOLDER": "មិនមានថតបែបនេះទេ",
        "NO_DIRECTORY": "មិនមានថតបែបនោះទេ",
        "INVALID_PATH": "ផ្លូវមិនត្រឹមត្រូវ",
        "OPEN": "បើក",
        "SAVE": "រក្សាទុក",
        "NUMBER": "#",
        "ADD" :"បន្ថែម",
        "CATEGORY" : "ប្រភេទ",
        "PRODUCT" : "ផលិតផល",
        "FROM_IMAGE": "តើអ្នកចង់បានរូបភាពពីណា?",
        "CHOOSE_ACTION": "ជ្រើសរើសសកម្មភាព",
        "FROM_CAMERA": "យកពីកាមេរ៉ា",
        "FROM_GALLERY": "យកពីវិចិត្រសាល",
        "WAIT": "សូមរង់ចាំ ....",
        "INSERT_CATEGORY_NAME": "សូមបញ្ចូលឈ្មោះប្រភេទ",
        "UPLOAD_CATEGORY_IMAGE": "សូមផ្ទុកឡើងនូវរូបភាពនៃកាព្យ",
        "SURE_ORDER_PAID": "តើអ្នកប្រាកដថាបញ្ជាទិញនេះត្រូវបានបង់ទេ?",
        "SURE_ACCEPT_ORDER" : "តើអ្នកយល់ព្រមទទួលយកការបញ្ជាទិញនេះទេ?",
        "SURE_DELIVER_COFFEE": "តើអ្នកបានចែកចាយកាហ្វេដែរឬទេ?",
        "CONFIRM": "អះអាង",
        "INSERT_PRODUCT_NAME": "សូមបញ្ចូលឈ្មោះរបស់ផលិតផល",
        "INSERT_PRODUCT_PRICE": "សូមបញ្ចូលតម្លៃរបស់ផលិតផល",
        "UPLOAD_PRODUCT_IMAGE": "សូមផ្ទុកឡើងនូវរូបភាពរបស់ផលិតផល",
        "UPDATE" : "ធ្វើឱ្យទាន់សម័យ",
        "DELETE"  : "លុប",
        "DELIVERED" : "បានបញ្ជូន",
        "PAID " : "បង់",
        "SIGNIN" : "ចូល",
        "ENTERUSERNAME" : "បញ្ចូលឈ្មោះអ្នកប្រើ",
        "ENTERPASSWORD" : "បញ្ចូលពាក្យសម្ងាត់",
        "LESS" : "តិចជាង",
        "MORE" : "ច្រើនទៀត"
    }
})