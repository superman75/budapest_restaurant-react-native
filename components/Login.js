import React, { Component} from 'react'
import { StyleSheet,
     Text, 
     View, 
     Image, 
     TouchableWithoutFeedback, 
     StatusBar, 
     TextInput,
     SafeAreaView,
     Keyboard, 
     TouchableOpacity, 
     AsyncStorage,
     ImageBackground,
     KeyboardAvoidingView, 
     Alert} from 'react-native';
import Counter from './counter/Counter';
import Waiter from './waiter/Waiter'
const Strings = require('./Strings');
export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
          username : 'counter123',
          userpassword : '123'
        };
    
      }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar barStyle="light-content"/>
                <ImageBackground style={styles.background}
                source={require('../images/my.jpg')}>

                <KeyboardAvoidingView style={styles.container}> 
                    <TouchableWithoutFeedback style={styles.container}
                        onPress={Keyboard.dismiss}>
                        <View style={styles.logoContainer}>
                            <View style={styles.logoContainer}>
                                <Image style={styles.logo} source={require('../images/logo.png')}>
                                </Image>                      
                            </View>
                            <View style={styles.infoContainer}>
                                <TextInput style={styles.input}
                                    placeholder={Strings.ENTERUSERNAME}
                                    placeholderTextColor='rgba(255,255,255,0.8)'
                                    keyboardType='email-address'
                                    returnKeyType='next'
                                    autoCorrect={false}
                                    onChangeText={(username)=>this.setState({username})}
                                    onSubmitEditing={()=>this.refs.txtPassword.focus()}
                                /> 
                                <TextInput style={styles.input}
                                    placeholder={Strings.ENTERPASSWORD}
                                    placeholderTextColor='rgba(255,255,255,0.8)'                            
                                    returnKeyType='go'
                                    secureTextEntry={true}
                                    autoCorrect={false}
                                    onChangeText={(userpassword)=>this.setState({userpassword})}
                                    ref={'txtPassword'}
                                />
                                <TouchableOpacity style={styles.buttonContainer} onPress={this.signIn}>
                                    <Text style={styles.buttonText}>{Strings.SIGNIN}</Text>
                                </TouchableOpacity> 
                            </View>                                     
                        </View>
                        
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
                </ImageBackground>
            </SafeAreaView>
       )
    }
    signIn = () => {
        fetch(Strings.BASE_URL+'login/3', {  
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                username: this.state.username,
                userpassword: this.state.userpassword,
            })
        }).then((response)=>response.json())
        .then((res)=>{
            if(res.status=='Success'){
                
                AsyncStorage.setItem('username',this.state.username); 
                AsyncStorage.setItem('userpassword',this.state.userpassword); 
                this.props.navigation.navigate('Counter');
            } else {
                Alert.alert(res.msg);
            }
        })
        .done();

    }
}
   

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    background : {
        flex: 1,
    },
    logoContainer: {
        alignItems : 'center',
        justifyContent : 'center',
        flex : 1
    },
    logo : {
        width : 150,
        height :150,
    },
    infoContainer: {
        // position : 'absolute',
        left : 0,
        right : 0,
        bottom : 0,
        height : 200,
        padding: 20,
        alignItems: 'center',
        // backgroundColor : 'red'
    },
    input : {
        paddingLeft: 20,
        height: 40,
        width: 500,
        backgroundColor: 'rgba(255,255,255,0.2)',
        color: '#FFF',
        paddingHorizontal: 10,
        marginBottom: 20,
        borderRadius: 10,
        fontSize: 18
    },
    buttonContainer: {
        width: 500,
        borderRadius: 10,
        backgroundColor: 'rgba(2,1,1,0.5)',
        paddingVertical: 15,
    },
    buttonText: {
        textAlign: 'center',
        color: 'rgb(250,250,250)',
        fontWeight: 'bold',
        fontSize: 18
    }


})