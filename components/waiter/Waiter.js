import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import MyTable from './MyTable';
import Product from './Product';
import Order from './Order';
import Feedback from './Feedback';
import Create from './Create'
import { createStackNavigator} from 'react-navigation';
import MyCategory from './MyCategory';
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

export default class Waiter extends Component {
  render() {
    return (
      <WaiterStack/>
    )
  }
}
const WaiterStack = createStackNavigator(
  {
    MyTable: {
      screen: MyTable,
      overrideBackPress: true,
      navigationOptions: () => ({
        title: "Table",
        headerVisible: false,
      }),
    },
    MyCategory : {
      screen : MyCategory,
      overrideBackPress: true,
      navigationOptions: () => ({
        title: "Category",
        headerVisible: true,
      }),
    },
    Product : {
      screen : Product,
      overrideBackPress: true,
      navigationOptions: () => ({
        title: "Product"
      }),
    },
    Order : {
      screen : Order,
      overrideBackPress: true,
      navigationOptions: () => ({
        title: "Orders"
      }),
    },
    Create : {
      screen : Create,
      overrideBackPress: true,
    },
    Feedback : {
      screen : Feedback,
      overrideBackPress: true
    }
  },
  {
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    },
    initialRouteName: 'MyTable',
  }
);