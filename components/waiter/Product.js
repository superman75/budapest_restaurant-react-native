import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Alert, AsyncStorage, FlatList, Dimensions, StyleSheet, ImageBackground, Image } from 'react-native'
import { Card, ListItem, Button } from 'react-native-elements';
import Create from './Create';
const Strings = require('../Strings');
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Warning:   Possible Unhandled Promise Rejection', 'Module RCTImageLoader']);
const numColumns = 4;
const formatData = (data, numColumns) => {
  const numberOfFullRows = Math.floor(data.length / numColumns);

  let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
  while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
    data.push({ key: `blank-${numberOfElementsLastRow}`, empty: true });
    numberOfElementsLastRow++;
  }

  return data;
};


export default class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products : []
    };
    
    this._loadInitialState().done();
  }
  _loadInitialState = async()=>{
    let categoryId = this.props.navigation.getParam('categoryId','1');
    fetch(Strings.BASE_URL+'products/' + categoryId,{
      method: 'GET'
    })
    .then(response=>response.json())
    .then(res=>{
      if(res.status=='Success'){
        this.setState({products: res.msg});
      } else {
        Alert.alert(res.msg);
      }
    })
  }
  renderItem = ({ item, index }) => {
    let imageurl = Strings.BASE_URL1 + '/images/assets/default-product.png';
    if(item.productimgurl !==null && item.productimgurl !== '') imageurl= Strings.BASE_URL1 + item.productimgurl;
    if (item.empty === true) {
      return <View  style={[styles.item, styles.itemInvisible]} />;
    }
    if (item.empty === true) {
      return <View  style={[styles.item, styles.itemInvisible]} />;
    }
    return (
      <View
      style={styles.item}>
      <TouchableOpacity style={styles.item} activeOpacity = { .5 } onPress={this.add.bind(this, item.id, item.productimgurl, item.productname, item.productdetail, item.productprice)}>      
      <Image 
        style={styles.item}
        source={{uri: imageurl}}
      />
      <Text style={styles.itemText}>{item.productname}{"\n"}${item.productprice}</Text>
      </TouchableOpacity>
      </View>
    );
  };
  render() {
    return (
      <ImageBackground style={styles.container}
      source={require('../../images/background.jpg')}>
        <FlatList
            data={formatData(this.state.products, numColumns)}
            style={styles.container}
            renderItem={this.renderItem}
            numColumns={numColumns}/>

      </ImageBackground>
    )
  }
  add = (id, url, name, detail, price) => {
    
    AsyncStorage.getItem('menu').then(res=>{
      const menu = res?JSON.parse(res):[]; 
      let index = menu.findIndex(e1=>e1.product_id===id);
      if(index==-1){
        menu.push({product_id: id, product_count: 1, product_name : name, productimgurl : url, product_detail : detail, product_price : price});
      } else {
        menu[index]={...menu[index], product_count : menu[index].product_count+1};
      }      
      AsyncStorage.setItem('menu',JSON.stringify(menu))
      .then(()=>{
        this.props.navigation.navigate('Create',{'productId': id});
      });
      
    });   
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,  
    marginHorizontal: 40,
    marginTop: 10,
    borderRadius: 10,
    width: Dimensions.get('window').width / numColumns-80, // approximate a square
    height: Dimensions.get('window').width / numColumns-80,
  },
  itemInvisible: {
    backgroundColor: 'transparent',
  },
  itemText: {
    position: 'absolute',
    color: '#fff',
    paddingHorizontal: 'auto',
    fontSize: 20,
    fontWeight: 'bold',
    bottom : 0,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    textAlign: 'center',
    width: '100%',
    backgroundColor : 'rgba(2,1,1,0.7)'
  },
})