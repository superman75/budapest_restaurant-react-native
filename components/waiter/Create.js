import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Alert, AsyncStorage, FlatList, Dimensions, StyleSheet, ImageBackground, Image } from 'react-native';                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
import { Card, ListItem, Button } from 'react-native-elements';
import Order from './Order';
import MyCategory from './MyCategory';
const Strings = require('../Strings');
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Warning:   Possible Unhandled Promise Rejection', 'Module RCTImageLoader']);
const numColumns = 1;
const formatData = (data, numColumns) => {
  const numberOfFullRows = Math.floor(data.length / numColumns);

  let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
  while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
    data.push({ key: `blank-${numberOfElementsLastRow}`, empty: true });
    numberOfElementsLastRow++;
  }
  return data;
};

export default class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
          product : {},
          menu: [],
          toalPrice : 0,
        };
      }
      componentWillMount(){

        this._loadInitialState().done();
    
      };    

    _loadInitialState = async()=>{
        const product_id = this.props.navigation.getParam('productId','1');
        AsyncStorage.getItem('menu').then(res=>{            
            this.setState({'menu' : JSON.parse(res)});
            let totalPrice = 0;
            this.state.menu.forEach(element => {
                totalPrice += element.product_count * element.product_price; 
            });
            this.setState({'totalPrice':  totalPrice});    
            
            let menu = JSON.parse(res);
            let index = menu.findIndex(e1=>e1.product_id == product_id);
            this.setState({'product': menu[index]});
        });
    }
    renderItem = ({ item, index }) => {
        let imageurl = Strings.BASE_URL1 + '/images/assets/default-product.png';
        if(item.productimgurl !==null && item.productimgurl !== '') imageurl= Strings.BASE_URL1 + item.productimgurl;
        if (item.empty === true) {
          return <View  style={[styles.item, styles.itemInvisible]} />;
        }
        return (
          
          <TouchableOpacity style={styles.touch} activeOpacity = { .5 } onPress={this.changeItem.bind(this,item.product_id,item.product_count)}>      
            <Image source={{uri: imageurl}} style={{width : 100, height:100}}></Image>
               <Text style={styles.nameText}>{item.product_name}</Text>   
            <Text style={styles.nameText}>${item.product_price}</Text>   
            <Text style={styles.countText}>{item.product_count}</Text>             
          </TouchableOpacity>
        );
      };
    render() {
        let imageurl = Strings.BASE_URL1 + '/images/assets/default-product.png';
        if(this.state.product.productimgurl !==null && this.state.product.productimgurl !== '') imageurl= Strings.BASE_URL1 + this.state.product.productimgurl;
        return (
            
            <ImageBackground source={require('../../images/mywall3.jpg')} style={styles.container}> 
                <View style={styles.container}>
                    <View stye={styles.leftPanel}>
                            <Image style={styles.photo} source={{uri: imageurl}}></Image>
                            <View style={{flexDirection: 'row', top : 20}}> 
                                <Text style={styles.productName}>{this.state.product.product_name}</Text>      
                                <Text style={styles.productPrice}>${this.state.product.product_price}</Text>              
                            </View>
                            <View style={{flexDirection: 'row', top: 50}} >
                                <TouchableOpacity style={styles.button} onPress={this.decreaseCount}><Text style={{fontSize : 25, color : '#FFF', textAlign : 'center',}}>-{Strings.LESS}</Text></TouchableOpacity>
                                <Text style={styles.count}>{this.state.product.product_count}</Text>
                                <TouchableOpacity style={styles.button} onPress={this.increaseCount}><Text style={{fontSize : 25, color : '#FFF',textAlign : 'center',}}>+{Strings.MORE}</Text></TouchableOpacity>
                            </View>
                            <Text style={styles.detail}>Details: {this.state.product.product_detail}</Text>
                    </View>
                    <View style={styles.rightPanel}>
                        <TouchableOpacity style={{backgroundColor : '#432', padding : 20}} onPress={()=>this.props.navigation.navigate('MyCategory')}>
                            <Text style={{textAlign : 'center', fontSize : 25, color : '#FFF'}}>+ Add New Item</Text>
                        </TouchableOpacity>
                        <FlatList
                            data={formatData(this.state.menu, numColumns)}
                            style={{flex : 1}}
                            renderItem={this.renderItem}
                            numColumns={numColumns}>
                        </FlatList>
                        <View style={{backgroundColor : '#432', padding : 10, alignItems : 'center'}}>
                            <Text style={{color : '#fff', fontSize : 20,}}>{Strings.TOTAL} {Strings.PRICE}: <Text style={{color : '#A60', fontSize : 25}}>${this.state.totalPrice}</Text></Text>
                            <TouchableOpacity onPress={this.sendOrder} style={{margin : 20, backgroundColor : '#211', borderRadius : 10, paddingHorizontal : 80, marginHorizontal : 20}}><Text style={{fontSize : 25, textAlign : 'center', color : '#FFF'}}>{Strings.CONFIRM}</Text></TouchableOpacity>
                            <TouchableOpacity onPress={()=>this.props.navigation.navigate('Order')} style={{margin : 20, backgroundColor : '#444', borderRadius : 10, paddingHorizontal : 80, marginHorizontal : 20}}><Text style={{fontSize : 25, textAlign : 'center', color : '#FFF'}}>{Strings.CANCEL}</Text></TouchableOpacity>
                        </View>

                    </View>
                            
                </View>
            
            </ImageBackground>
        )
    }
    increaseCount = () => {
        let count = this.state.product.product_count + 1;
        let menu = [...this.state.menu];
        let index = menu.findIndex(e1=>e1.product_id===this.state.product.product_id);
        menu[index]={...menu[index], product_count : count};
        this.setState({menu : menu},()=>{
            AsyncStorage.setItem('menu',JSON.stringify(menu));
            let totalPrice = 0;
            this.state.menu.forEach(element => {
                totalPrice += element.product_count * element.product_price; 
            });
            this.setState({'totalPrice':  totalPrice});  
            this.setState({'product': menu[index]});
        });
    }
    decreaseCount = () => {

        let count = this.state.product.product_count - 1;
        if(count<0){ return;}
        let menu = [...this.state.menu];
        let index = menu.findIndex(e1=>e1.product_id===this.state.product.product_id);
        if(count==0){
            menu.splice(index,1);
            index = 0;
            if(menu.length == 0) {
                this.props.navigation.navigate('Order');
                return;
            }

        } else {
            menu[index]={...menu[index], product_count : count};
        }        
        this.setState({menu : menu},()=>{
            AsyncStorage.setItem('menu',JSON.stringify(menu));
            let totalPrice = 0;
            this.state.menu.forEach(element => {
                totalPrice += element.product_count * element.product_price; 
            });
            this.setState({'totalPrice':  totalPrice});  
            this.setState({'product': menu[index]});
        });
    }
    sendOrder = async() =>{
        let submenu = [];
        this.state.menu.forEach(element => {
            submenu.push({product_id : element.product_id, product_count : element.product_count});
        });
        let username = await AsyncStorage.getItem('username');
        let userpassword  = await AsyncStorage.getItem('userpassword');
        let table_id = await AsyncStorage.getItem('tableId');
        fetch(Strings.BASE_URL + 'order',{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                username: username,
                userpassword: userpassword,
                ordercontent : JSON.stringify(submenu),
                table_id : table_id
            })
        }).then(response=>response.json())
        .then(res=>{
            if(res.status === 'Success')this.props.navigation.navigate('Order');
            else{
                Alert.alert("Error : " + res.msg);
            }
            
        })
    }
    changeItem = async(product_id, count) =>{
        let menu = [...this.state.menu];
        let index = await menu.findIndex(e1=>e1.product_id === product_id);
        this.setState({'product': menu[index]});
    }
}


const styles = StyleSheet.create({
    container : {
        flexDirection : 'row',
        fontSize: 20, 
        flex : 1,
        height: Dimensions.get('window').height,
    },
    leftPanel: {
        width: Dimensions.get('window').width*0.6, 
        flexDirection : 'column',
        alignItems: 'center',
        padding: 20,
    },
    photo: {
        width: 360,
        height : 200,
        borderRadius: 50,
        marginHorizontal : 100,
        marginVertical: 30,
        borderColor: '#ece4df'
    },
    rightPanel: {
        width: Dimensions.get('window').width*0.4,
        backgroundColor : '#875534',
    },
    productName: {
        alignSelf: 'flex-start',
        fontSize: 25,
        marginLeft: 50,
        color : '#fff'
    },
    productPrice : {
        alignSelf: 'flex-end',
        fontSize: 25,
        marginLeft: 150,
        color : '#fff'
    },
    button : {
        fontSize : 25,
        marginHorizontal: 60,
        borderRadius : 10,
        backgroundColor: '#211',
        color :'#FFF',
        width : 150,
    },
    count : {
        fontSize : 25,
        backgroundColor : '#FFF',
        color : '#211',
        width : 50,
        textAlign: 'center',
    },
    detail : {
        top: 80,
        left : 50,
        fontSize : 25,
        color : '#fff'
    },
    item: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    itemInvisible: {
        backgroundColor: 'transparent',
    },
    nameText: {
        color : 'white',
        fontSize : 20,
        marginLeft : 10,
    },
    countText : {
        fontSize : 20,
        width : 40,
        borderRadius : 20,
        textAlign: 'center',
        backgroundColor : 'white',
    },
    touch : {
        flexDirection : 'row',
        height : 100, 
        paddingRight: 20,
        justifyContent : 'space-between',
        alignItems: 'center', 
        width : '100%', 
        borderColor: 'white', 
        borderTopWidth : 2
    }
});