import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Alert, FlatList, Dimensions, StyleSheet, ImageBackground, Image, AsyncStorage} from 'react-native';
import { Card, ListItem,Button } from 'react-native-elements';
import MyCategory from './MyCategory';
const Strings = require('../Strings');
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Warning:   Possible Unhandled Promise Rejection', 'Module RCTImageLoader']);
const numColumns = 2;
const intiaiNumber = 3;
const formatData = (data, numColumns) => {
  const numberOfFullRows = Math.floor(data.length / numColumns);

  let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
  while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
    data.push({ key: `blank-${numberOfElementsLastRow}`, empty: true });
    numberOfElementsLastRow++;
  }

  return data;
};


export default class Order extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orders : [],
      username : '',
      userpassword : ''
    };    

  }
  componentWillMount(){

    this._loadInitialState().done();

  };
  _loadInitialState = async()=>{

    
    AsyncStorage.getItem('username').then(username=>this.setState({username: username}));
    AsyncStorage.getItem('userpassword').then(userpassword=>this.setState({userpassword : userpassword}));

    let tableId = this.props.navigation.getParam('tableId','1');
    
    fetch(Strings.BASE_URL+'order/' + tableId,{
      method: 'GET'
    })
    .then(response=>response.json())
    .then(res=>{
      if(res.status=='Success'){
        this.setState({orders: res.msg});
      } else {
        Alert.alert(res.msg);
      }
    })
    setInterval(()=>{

      fetch(Strings.BASE_URL+'order/' + tableId,{
        method: 'GET'
      })
      .then(response=>response.json())
      .then(res=>{
        if(res.status=='Success'){
          this.setState({orders: res.msg});
        } else {
          Alert.alert(res.msg);
        }
      })
    },2000);
  }

  renderItem = ({ item, index }) => {
    let statusIcon;
    let paidTime = null;
    if(item.status==0){
      statusIcon = <Image style={styles.statusIcon} source={require('../../images/bell.png')}/>
    } else if(item.status == 1){
      statusIcon = <Image style={styles.statusIcon}  source={require('../../images/order_icon.png')}/>
    } else if(item.status == 2){
      statusIcon = <Image style={styles.statusIcon}  source={require('../../images/coffee_cup.png')}/>
    } else {
      statusIcon = <Image style={styles.statusIcon}  source={require('../../images/dollar.png')}/>
      paidTime = <Text style={styles.paidTimeText}>{Strings.PAID_AT} {item.updated_at}</Text>;      
    }
    if (item.empty === true) {
      return <View  style={[styles.item, styles.itemInvisible]} />;
    }
    return (
      
      <ImageBackground 
      style={styles.item}
      source={require('../../images/ticket.png')}>
      <TouchableOpacity style={styles.touch} onPress={this.changeStatus.bind(this, item.order_id,item.status,item.table_id)}>
      <Text style={styles.orderIdText}>{Strings.TABLE} {item.table_id}</Text>      
      <Text style={styles.tableIdText}>#{item.order_id}</Text>  
      <View style={styles.flatList}>
        <FlatList
            data={item.products} 
            style={{zIndex: 5, elevation: 5, flex : 1}}
            scrollEnabled={true}   
            initialNumToRender={intiaiNumber}      
            listKey={(item, index) => 'D' + index.toString()}
            renderItem={({item})=><Text   style={styles.productItemText}>{item.product_name}(${item.product_price})×{item.product_count}=${item.product_price*item.product_count}</Text>}/>
      </View>
      <Text style={styles.totalPriceText}>{Strings.TOTAL}: ${item.total_price}</Text>
      <Text style={styles.createdTimeText}>{Strings.ORDERED_AT} {item.created_at}</Text>      
      {paidTime}
      </TouchableOpacity>
      {statusIcon}  
      </ImageBackground >
    );
  };

  render() {
    return (
      <ImageBackground style={styles.container}
      source={require('../../images/background.jpg')}>
        <Image source={require('../../images/coffee_deco.png')}
          style={styles.coffee}
        ></Image>
        <FlatList
            data={formatData(this.state.orders, numColumns)}
            style={styles.container}
            renderItem={this.renderItem}                     
            listKey={(item, index) => 'D' + index.toString()}
            numColumns={numColumns}/>
      </ImageBackground>
    )
  }
  changeStatus = (id, status, tableId) => {


    let alertMessage = "";
    if(status===0){
      alertMessage = Strings.SURE_ACCEPT_ORDER;
    } else if (status === 1){
      alertMessage = Strings.SURE_DELIVER_COFFEE;
    } else if (status === 2){
      alertMessage = Strings.SURE_ORDER_PAID;
    } else return;
    Alert.alert( 
      alertMessage,
      "",
      [
        {text : Strings.NO, onPress:()=>console.log('Cancel Pressed.'), style: 'cancel'},
        {text : Strings.YES, onPress: ()=>{
          fetch(Strings.BASE_URL+'order/promote/' + id, {  
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              username: this.state.username,
              userpassword : this.state.userpassword,
            })
          }).then(response=>response.json())
          .then(res=>{
            if(res.status=='Success'){
              fetch(Strings.BASE_URL+'order/' + tableId,{
                method: 'GET'
              })
              .then(response=>response.json())
              .then(res=>{
                if(res.status=='Success'){
                  this.setState({orders: res.msg});
                  let orders = this.state.orders;
                  orders.forEach(element => {
                      let totalprice = 0;
                      element.products.forEach(product => {
                        totalprice = totalprice + product.product_count*product.product_price;
                      });
                      element.totalPrice = totalprice;
                  });
                  Alert.alert(JSON.stringify(orders));
                  this.setState({orders : orders});
                } else {
                  Alert.alert(res.msg);
                }
              })
            } else {
              Alert.alert(res.msg);
            }
          })      
        }}
      ],
      {cancelable: false}
    )
    
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 30,
    width: Dimensions.get('window').width / numColumns-60,
    height: Dimensions.get('window').width / numColumns-200,
  },  
  touch: {
    flex: 1,  
    width: Dimensions.get('window').width / numColumns-60,
    height: Dimensions.get('window').width / numColumns-200,
  },
  itemInvisible: {
    backgroundColor: 'transparent',
  },
  tableIdText: {
    position: 'absolute',
    left : 30,
    top : 30,
    color : 'rgb(252,205,168)',
    fontWeight: 'bold',
    fontSize : 20,
  },
  
  orderIdText: {
    position: 'absolute',
    right : 20,
    top : 30,
    color : 'black',
    fontSize : 17,
    fontWeight: 'bold',
  },
  flatList: {
    position: 'absolute',
    left : 10,
    top : 70,
    height: 100,
    flex : 1,
    alignItems : 'flex-start'
  },
  productItemText: {
    color : '#af7b4e',
    fontSize : 12,
    alignSelf: 'flex-start',
    fontStyle : 'italic'
  },  
  statusIcon: {
    width: 50,
    height: 50,
    position: 'absolute',
    left : -25,
    top : -25,
  },
  totalPriceText: {
    position : 'absolute',
    color : '#c89565',
    fontSize : 20,
    fontWeight: '500',
    alignSelf: 'flex-start',
    left : 10,
    top: 180,
  }, 
  createdTimeText: {
    position : 'absolute',
    color : '#da8651',
    fontSize : 15,
    alignSelf: 'flex-end',
    top: 200,
    right : 10,
  },
  paidTimeText: {
    position : 'absolute',
    color : '#da8651',
    fontSize : 15,
    alignSelf: 'flex-end',
    top: 220,
    right : 10,
  },

  coffee: {
    position: 'absolute',
    bottom : 0,
    right : 0,
  }
})