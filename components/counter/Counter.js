import React, { Component } from 'react';
import { View, Text } from 'react-native';

import {createBottomTabNavigator, createStackNavigator} from 'react-navigation';
import Order from './Order';
import MyTable from './MyTable';
import MyCategory from './MyCategory.js';
import Orders from './Orders';
import Product from './Product';

const Stack1 = createStackNavigator(
  {
    MyCategory: {
      screen: MyCategory,
      overrideBackPress: true
    },
    Product : {
      screen : Product,
      overrideBackPress: true
    }
  },
  {
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    },
    initialRouteName: 'MyCategory',
  }
);
const Stack2 = createStackNavigator(
  {
    MyTable: {
      screen: MyTable,
      overrideBackPress: true
    },
    Order : {
      screen : Order,
      overrideBackPress: true
    }
  },
  {
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    },
    initialRouteName: 'MyTable',
  }
);
const CounterTabs = createBottomTabNavigator({
    Category : {
      screen : Stack1,
    },
    Orders : {
      screen : Orders,
    },
    Table : {
      screen : Stack2,
    },
  },
  {
    tabBarPosition: 'bottom',
    animationEnabled : true,
    swipeEnabled: false,
    lazy : false,
    tabBarOptions : {
      activeTintColor : '#986645',
      inactiveTintColor: 'white',
      labelStyle: {
        fontSize: 30,
      },
      style : {
        backgroundColor : 'rgba(2,1,1,1)',
      }
    }
  });
export default CounterTabs;
