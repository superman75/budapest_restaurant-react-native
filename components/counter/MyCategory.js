import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Alert, AsyncStorage, FlatList, Dimensions, StyleSheet, ImageBackground, Image } from 'react-native'
import { Card, ListItem, Button } from 'react-native-elements';
import Product from './Product';
const Strings = require('../Strings');
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Warning:   Possible Unhandled Promise Rejection', 'Module RCTImageLoader']);
const numColumns = 4;
const formatData = (data, numColumns) => {
  const numberOfFullRows = Math.floor(data.length / numColumns);

  let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
  while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
    data.push({ key: `blank-${numberOfElementsLastRow}`, empty: true });
    numberOfElementsLastRow++;
  }

  return data;
};

export default class MyCategory extends Component {

  constructor(props) {
    super(props);
    this.state = {
      categories : []
    };
    this.props.navigation.state.key = 'category';
    this._loadInitialState().done();

  }
  _loadInitialState = async()=>{
    
    fetch(Strings.BASE_URL+'categories',{
      method: 'GET'
    })
    .then(response=>response.json())
    .then(res=>{
      if(res.status=='Success'){
        this.setState({categories: res.msg});
      } else {
        Alert.alert(res.msg);
      }
    })
  }
  renderItem = ({ item, index }) => {
    let imageurl = '../../images/cook.jpg';
    if (item.empty === true) {
      return <View  style={[styles.item, styles.itemInvisible]} />;
    }
    return (
      <View 
      style={styles.item}
      source={require(imageurl)}
      >
       <TouchableOpacity style={styles.item} activeOpacity = { .5 } onPress={ this.toProduct.bind(this, item.id) }>     
        <Image
          style={styles.item}
          source={require(imageurl)}>
        </Image>
        </TouchableOpacity> 
      <Text style={styles.itemText}>{item.categoryname  }</Text>      

      </View >
    );
  };
  render() {
    return (
      <ImageBackground style={styles.container}
      source={require('../../images/background.jpg')}>
        <FlatList
            data={formatData(this.state.categories, numColumns)}
            style={styles.container}
            renderItem={this.renderItem}
            numColumns={numColumns}/>

      </ImageBackground>
    )
  }
  toProduct = (id) => {
    this.props.navigation.navigate("Product",{categoryId : id});
  }

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,  
    marginHorizontal: 40,
    marginTop: 10,
    borderRadius: 10,
    width: Dimensions.get('window').width / numColumns-80, // approximate a square
    height: Dimensions.get('window').width / numColumns-50,
  },
  itemInvisible: {
    backgroundColor: 'transparent',
  },
  itemText: {
    color: '#fff',
    fontSize: 25,
    fontWeight: 'bold'
  },
})