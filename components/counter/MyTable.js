import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Alert, FlatList, Dimensions, StyleSheet, ImageBackground, Image,AsyncStorage } from 'react-native';
import { Card, ListItem, Button } from 'react-native-elements';
import Order from './Order';
const Strings = require('../Strings');
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Warning:   Possible Unhandled Promise Rejection', 'Module RCTImageLoader']);
const numColumns = 4;
const formatData = (data, numColumns) => {
  const numberOfFullRows = Math.floor(data.length / numColumns);

  let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
  while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
    data.push({ key: `blank-${numberOfElementsLastRow}`, empty: true });
    numberOfElementsLastRow++;
  }

  return data;
};


export default class MyTable extends Component {

  constructor(props) {
    super(props);
    this.state = {
      tables : []
    };
    this._loadInitialState().done();
    console.disableYellowBox = true; 

  }
  _loadInitialState = async()=>{
    
    fetch(Strings.BASE_URL+'tables',{
      method: 'GET'
    })
    .then(response=>response.json())
    .then(res=>{
      if(res.status=='Success'){
        this.setState({tables: res.msg});
      } else {
        Alert.alert(res.msg);
      }
    })
  }

  renderItem = ({ item, index }) => {
    if (item.empty === true) {
      return <View  style={[styles.item, styles.itemInvisible]} />;
    }
    return (

      <TouchableOpacity activeOpacity = { .5 } onPress={ this.toCategory.bind(this, item.id) }>      
        <ImageBackground 
        style={styles.item}
        source={require('../../images/table.png')}
        >
         <Text style={styles.itemText}>{item.tablename}</Text>  
        </ImageBackground >    
      </TouchableOpacity>
    );
  };
  render() {
    return (
      <ImageBackground style={styles.container}
      source={require('../../images/background.jpg')}>
        <Image source={require('../../images/coffee_deco.png')}
          style={styles.coffee}
        ></Image>
        <FlatList
            data={formatData(this.state.tables, numColumns)}
            style={styles.container}
            renderItem={this.renderItem}
            numColumns={numColumns}/>

      </ImageBackground>
    
      )
  }
  toCategory = (id) => {
    AsyncStorage.setItem('tableId',id+'');
    this.props.navigation.navigate("Order",{tableId : id});
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    marginHorizontal: 40,
    marginVertical: 20,
    width: Dimensions.get('window').width / numColumns-80, // approximate a square
    height: Dimensions.get('window').width / numColumns-80,
  },
  itemInvisible: {
    backgroundColor: 'transparent',
  },
  itemText: {
    color: '#fff',
    fontSize: 40,
    fontWeight: 'bold'
  },
  coffee: {
    position: 'absolute',
    bottom : 0,
    right : 0,
  
  }
})